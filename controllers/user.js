const User = require('../models/user');

exports.read = (req, res) => {
    const userId = req.params.id;
    console.log(req.params.id);
    
    User.findById(userId).exec((err, user) => {
        if (err || !user) {
            return res.status(400).json({
                error: 'User not found'
            });
        }
        user.hashed_password = undefined;
        user.salt = undefined;
        res.json(user);
        console.log(user._id);
    });
};

exports.readachievements = (req,res) => {
     const userId = req.params.id;
    console.log(req.params.id);
    
    User.findById(userId).exec((err, user) => {
        if (err || !user) {
            return res.status(400).json({
                error: 'User not found'
            });
        }
        user.hashed_password = undefined;
        user.salt = undefined;
        res.json(user.achievements);
        console.log(user._id);
        
    });
};


exports.update = (req, res) => {
     console.log('UPDATE USER - req.user', req.user, 'UPDATE DATA', req.body);
     console.log('UPDATE USER Achievements - req.user', req.user.achievements, 'UPDATE DATA', req.body.achievements);
    const { role, image, name, password, year, branch, description,linkedin,facebook,github,instagram,twitter,whatsapp,native,curricular  } = req.body;

    User.findOne({ _id: req.user._id }, (err, user) => {
        if (err || !user) {
            return res.status(400).json({
                error: 'User not found'
            });
        }
        if (!name) {
            return res.status(400).json({
                error: 'Name is required'
            });
        } else {
            user.name = name;
        }

        if (!image) {
            return res.status(400).json({
                error: 'Profile Image is required'
            });
        } else {
            user.image = image;
        }

        if (!year) {
            return res.status(400).json({
                error: 'Year is required'
            });
        } else {
             user.year = year;
        }

        if (!branch) {
            return res.status(400).json({
                error: 'Branch is required'
            });
        } else {
             user.branch = branch;
        }

        if (!description) {
            return res.status(400).json({
                error: 'Write something about yourself'
            });
        } else {
             user.description = description;
        }

        
             user.linkedin = linkedin;
        
             user.facebook = facebook;
        
             user.instagram = instagram;
        
             user.github = github;
        
             user.twitter = twitter;
        
             user.whatsapp = whatsapp;
        
             user.native = native;
        
             user.curricular = curricular;


        if (password) {
            if (password.length < 6) {
                return res.status(400).json({
                    error: 'Password should be min 6 characters long'
                });
            } else {
                user.password = password;
            }
        }

        user.save((err, updatedUser) => {
            if (err) {
                console.log('USER UPDATE ERROR', err);
                return res.status(400).json({
                    error: 'User update failed'
                });
            }
            updatedUser.hashed_password = undefined;
            updatedUser.salt = undefined;
            res.json(updatedUser);
        });
    });
};

exports.addachievements = (req,res,next) => {
    const userId = req.params.id;
    console.log(req.params.id);
    
    User.findById(userId).exec((err, user) => {
        if (err || !user) {
            return res.status(400).json({
                error: 'User not found'
            });
        } else {
        console.log(user._id);
       
            req.body.image = user._id;
            req.body.about = user._id;
             user.achievements.push(req.body);
             user.save()
            .then(( user) => {
                 User.findById( user._id)
                .then(( user) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json( user);
                })            
            }, (err) => next(err));
        }
    });
};