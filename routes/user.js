const express = require('express');
const router = express.Router();
const User = require('../models/user');

// import controller
const { requireSignin, } = require('../controllers/auth');
const { read, update, readachievements, addachievements } = require('../controllers/user');

router.get('/user/:id', read);
router.put('/user/update', requireSignin, update);
router.get('/user/:id/achievements', readachievements)
router.post('/user/:id/achievements', addachievements)
router.get('/students', function(req, res) {    
    User.find({}, function (err, users) {
        var data = users.filter(x=> x.role==="Student");
        res.send(data);
        console.log(data.length);
    });
});

router.get('/professors', function(req, res) {    
    User.find({}, function (err, users) {
        var data = users.filter(x=> x.role==="Professor");
        res.send(data);
        console.log(data.length);
    });
});
router.get('/alumnies', function(req, res) {    
    User.find({}, function (err, users) {
        var data = users.filter(x=> x.role==="Alumni");
        res.send(data);
        console.log(data.length);
    });
});

module.exports = router;