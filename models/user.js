const mongoose = require('mongoose');
const crypto = require('crypto');

//achievement Schema
const achievementSchema = new mongoose.Schema(
    {
        image:{
            type: String,
            trim: true,
        },
        about: {
            type: String,
            max: 100
        }
    },
    { timestamps: true }
);


// user schema
const userScheama = new mongoose.Schema(
    {
        image:{
            type: String,
            default: "https://www.cornwallbusinessawards.co.uk/wp-content/uploads/2017/11/dummy450x450.jpg",
        },
        name: {
            type: String,
            trim: true,
            required: true,
            max: 32
        },
        role: {
            type: String,
            trim: true,
            required: true,
            max: 32
        },
        email: {
            type: String,
            trim: true,
            required: true,
            unique: true,
            lowercase: true
        },
        year: {
            type: String,
            trim: true,
            required: false,
            max: 32
        },
        branch: {
            type: String,
            trim: true,
            required: false,
            max: 200
        },
        description: {
            type: String,
            trim: true,
            required: false,
            max: 200
        },
        linkedin: {
            type: String,
            trim: true,
            required: false,
            max: 200
        },
        facebook: {
            type: String,
            trim: true,
            required: false,
            max: 200
        },
        instagram: {
            type: String,
            trim: true,
            required: false,
            max: 200
        },
        github: {
            type: String,
            trim: true,
            required: false,
            max: 200
        },
        twitter: {
            type: String,
            trim: true,
            required: false,
            max: 200
        },
        whatsapp: {
            type: String,
            trim: true,
            required: false,
            max: 200
        },
        native: {
            type: String,
            trim: true,
            required: false,
            max: 200
        },
        curricular: {
            type: String,
            trim: true,
            required: false,
            max: 200
        },
        achievements: [achievementSchema],
        hashed_password: {
            type: String,
            required: true
        },
        salt: String,
        role: {
            type: String,
            default: 'Student'
        },
        resetPasswordLink: {
            data: String,
            default: ''
        }
    },
    { timestamps: true }
);

// virtual
userScheama
    .virtual('password')
    .set(function(password) {
        this._password = password;
        this.salt = this.makeSalt();
        this.hashed_password = this.encryptPassword(password);
    })
    .get(function() {
        return this._password;
    });

// methods
userScheama.methods = {
    authenticate: function(plainText) {
        return this.encryptPassword(plainText) === this.hashed_password; // true false
    },

    encryptPassword: function(password) {
        if (!password) return '';
        try {
            return crypto
                .createHmac('sha1', this.salt)
                .update(password)
                .digest('hex');
        } catch (err) {
            return '';
        }
    },

    makeSalt: function() {
        return Math.round(new Date().valueOf() * Math.random()) + '';
    }
};

module.exports = mongoose.model('User', userScheama);